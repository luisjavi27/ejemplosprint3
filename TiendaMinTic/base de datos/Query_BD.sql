CREATE DATABASE tiendaMintic;
USE tiendaMintic;

CREATE TABLE `tiendamintic`.`usuarios` (
  `idusuarios` INT NOT NULL AUTO_INCREMENT,
  `usuario` VARCHAR(45) NOT NULL,
  `tipousuario` VARCHAR(45) NOT NULL,
  `clave` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idusuarios`),
  UNIQUE INDEX `idUsuariosTienda_UNIQUE` (`idusuarios` ASC) VISIBLE,
  UNIQUE INDEX `usuario_UNIQUE` (`usuario` ASC) VISIBLE);

INSERT INTO usuarios(usuario, clave, tipousuario) values
("LUIS","1", "Operador"),
("ADMIN","ADMIN123", "Administrador");

CREATE TABLE `tiendamintic`.`producto` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `cantidad` INT NOT NULL,
  `precio` DOUBLE NOT NULL,
  `categoria` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE);

INSERT INTO producto(nombre, cantidad, precio, categoria) values
("JABON AZUL", 10, 2000, "ASEO"),
("ARROZ", 5700, 9, "VIVERES"),
("ARROZ GRANEL", 200, 1000, "VIVERES"),
("AZUCAR GRANEL", 300, 900, "VIVERES"),
("JABON AZUL", 10, 2000, "ASEO"),
("LIMPIDO", 100, 2000, "ASEO"); 


