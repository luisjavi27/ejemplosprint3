/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author luisj
 */
public class ConsultaProducto extends conexionDb{
    PreparedStatement ps;
    ResultSet rs;
    
    public boolean ingresar(Producto producto){
        Connection conexion= getConnection();
        
        try{
            ps = conexion.prepareStatement("insert into producto(nombre, cantidad, precio, categoria) values (?, ?, ?, ?)");
            ps.setString(1, producto.getNombre());
            ps.setInt(2, producto.getCantidad());
            ps.setDouble(3, producto.getPrecio());
            ps.setString(4, producto.getCategoria());
            int consulta= ps.executeUpdate();
        
            if( consulta>0){return true;}
            else{return false;}
            
        }catch(Exception except){
        
            System.out.println("Error "+ except);
        
            return false;
        }finally{
            try{ conexion.close();}
                catch(Exception except){
                    System.out.println("Error "+ except);
                }
        
        }
    }
    
    public boolean buscar(Producto producto){
        
        Connection conexion= getConnection();
        
        try{
           
            ps = conexion.prepareStatement("select * from producto where id=?");
            ps.setInt(1, producto.getId());
            
            rs= ps.executeQuery();
            
        
            if( rs.next()){
                
                producto.setId(rs.getInt("Id"));
                producto.setNombre(rs.getString("Nombre"));
                producto.setCantidad(rs.getInt("Cantidad"));
                producto.setPrecio(rs.getDouble("Precio"));
                producto.setCategoria(rs.getString("Categoria"));
                return true;
            }
            else{
                return false;
            }
            
        }catch(Exception except){
        
            System.out.println("Error "+ except);
        
            return false;
        }finally{
            try{ conexion.close();}
                catch(Exception except){
                    System.out.println("Error "+ except);
                }
        
        }
    }
    
    public boolean actualizar(Producto producto){
        
        Connection conexion= getConnection();
        
        try{
            int consulta;
          
            if (!"".equals(producto.getNombre())){
                ps=conexion.prepareStatement("update producto set nombre=\""+producto.getNombre()+"\" where id="+String.valueOf(producto.getId()));
                consulta=ps.executeUpdate();
                if( consulta<0){return false;}
            }
            
            
            if (-1!=producto.getCantidad()){
                System.out.println("cantidadcp: "+producto.getCantidad()+"-");
                ps=conexion.prepareStatement("update producto set cantidad=\""+producto.getCantidad()+"\" where id="+String.valueOf(producto.getId()));
                consulta=ps.executeUpdate();
                if( consulta<0){return false;}
            }
            
            if (-1!=producto.getPrecio()){
                ps=conexion.prepareStatement("update producto set precio=\""+String.valueOf(producto.getPrecio())+"\" where id="+String.valueOf(producto.getId()));
                consulta=ps.executeUpdate();
                if( consulta<0){return false;}
            }
            
            if (!"".equals(producto.getCategoria())){
                ps=conexion.prepareStatement("update producto set categoria=\""+producto.getCategoria()+"\" where id="+String.valueOf(producto.getId()));
                consulta=ps.executeUpdate();
                if( consulta<0){return false;}
            }
            
                   
                       
            return true;
            
        }catch(Exception except){
        
            System.out.println("Error "+ except);
        
            return false;
        }finally{
            try{ conexion.close();}
                catch(Exception except){
                    System.out.println("Error "+ except);
                }
        
        }
        
    }
    
    public boolean eliminar(Producto producto){
        
        Connection conexion= getConnection();
        
        try{
            ps = conexion.prepareStatement("delete from producto where id=?");
            ps.setInt(1, producto.getId());
            
            int consulta= ps.executeUpdate(); 
            
            if( consulta>0){return true;}
            else{return false;}
            
        }catch(Exception except){
        
            System.out.println("Error "+ except);
        
            return false;
        }finally{
            try{ conexion.close();}
                catch(Exception except){
                    System.out.println("Error "+ except);
                }
        }
        
    }
    
}
