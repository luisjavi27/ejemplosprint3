/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;



import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;
/**
 *
 * @author luisj
 */
public class conexionDb {

    public static final String URL = "jdbc:mysql://localhost:3306/tiendaMintic?autoReconnet=true&useSSL=false";
    public static final String usuario = "root";
    public static final String contraseña = "1234";

    public Connection getConnection() {
        Connection conexion = null;

        try {

            Class.forName("com.mysql.jdbc.Driver");
            conexion = (Connection) DriverManager.getConnection(URL, usuario, contraseña);
            //JOptionPane.showMessageDialog(null, "Conexion exitosa"); // Opcional

        } catch (Exception except) {
            System.out.println("Error " + except);
            JOptionPane.showMessageDialog(null, "Error en la conexión con la BD\n"+except); // Opcional
        }
        return conexion;
    }

}
