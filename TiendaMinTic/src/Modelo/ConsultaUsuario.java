/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author c.comercial4
 */
public class ConsultaUsuario extends conexionDb {
  PreparedStatement ps;
    ResultSet rs;
    
    public boolean ingresarNuevoU(Usuario usuario){
        Connection conexion= getConnection();
        
        try{
            
            
            System.out.println("usuario:");
            ps = conexion.prepareStatement("insert into usuarios(usuario, clave, tipousuario) values (?, ?, ?)");
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getClave());
            ps.setString(3, usuario.getTipo());
            int consulta= ps.executeUpdate();
        
            if( consulta>0){return true;}
            else{return false;}
            
        }catch(Exception except){
        
            System.out.println("Error "+ except);
        
            return false;
        }finally{
            try{ conexion.close();}
                catch(Exception except){
                    System.out.println("Error "+ except);
                }
        
        }
    }
    
    public boolean buscar(Usuario usuario){
        
        Connection conexion= getConnection();
        
        try{
           
            ps = conexion.prepareStatement("select*from usuarios where usuario=? and clave=?");
            ps.setString(1, usuario.getUsuario());
            ps.setString(2, usuario.getClave());
            rs= ps.executeQuery();
            
            if( rs.next()){
                
                usuario.setId(rs.getInt("idusuarios"));
                return true;
            } else{
                return false;
            }
        
            //return rs.next();
            
        }catch(Exception except){
        
            System.out.println("Error "+ except);
        
            return false;
        }finally{
            try{ conexion.close();}
                catch(Exception except){
                    System.out.println("Error "+ except);
                }
        
        }
    }
    
    public boolean buscarU(Usuario usuario){
        
        Connection conexion= getConnection();
        
        try{
           
            ps = conexion.prepareStatement("select*from usuarios where idusuarios=?");
            
            ps.setInt(1, usuario.getId());
            
            rs= ps.executeQuery();
            if( rs.next()){
                
                
                usuario.setTipo(rs.getString("tipousuario"));
                usuario.setUsuario(rs.getString("usuario"));
                return true;
            }
            return rs.next();
            
        }catch(Exception except){
        
            System.out.println("Error "+ except);
        
            return false;
        }finally{
            try{ conexion.close();}
                catch(Exception except){
                    System.out.println("Error "+ except);
                }
        
        }
    }
    
    
    public boolean actualizarU(Usuario usuario){
        
        Connection conexion= getConnection();
        
        try{
            int consulta;
           
            if (!"".equals(usuario.getUsuario())){
                ps=conexion.prepareStatement("update usuarios set usuario=\""+usuario.getUsuario()+"\" where idusuarios="+String.valueOf(usuario.getId()));
                consulta=ps.executeUpdate();
                if( consulta<0){return false;}
            }
            
            
            if (!"".equals(usuario.getClave())){
                ps=conexion.prepareStatement("update usuarios set clave=\""+usuario.getClave()+"\" where idusuarios="+String.valueOf(usuario.getId()));
                consulta=ps.executeUpdate();
                if( consulta<0){return false;}
            }
            
            
            if (!"".equals(usuario.getTipo())){
                ps=conexion.prepareStatement("update usuarios set tipousuario=\""+usuario.getTipo()+"\" where idusuarios="+String.valueOf(usuario.getId()));
                consulta=ps.executeUpdate();
                if( consulta<0){return false;}
            }
            
                   
                       
            return true;
            
        }catch(Exception except){
        
            System.out.println("Error "+ except);
        
            return false;
        }finally{
            try{ conexion.close();}
                catch(Exception except){
                    System.out.println("Error "+ except);
                }
        
        }
        
    }
    
    public boolean eliminarU(Usuario usuario){
        
        Connection conexion= getConnection();
        
        try{
            ps = conexion.prepareStatement("delete from usuarios where idusuarios=?");
            ps.setInt(1, usuario.getId());
            
            int consulta= ps.executeUpdate(); 
            
            if( consulta>0){return true;}
            else{return false;}
            
        }catch(Exception except){
        
            System.out.println("Error "+ except);
        
            return false;
        }finally{
            try{ conexion.close();}
                catch(Exception except){
                    System.out.println("Error "+ except);
                }
        }
        
    }
    
    
    
    
    
}
