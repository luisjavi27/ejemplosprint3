/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author luisj
 */
public class Producto {
    
private String nombre, categoria;
double precio;
private int cantidad, id;

public Producto(String nombre, int cantidad, double precio){
    this.nombre=nombre;
    this.cantidad=cantidad;
    this.precio=precio;
}

public Producto(){
    
}

public int getId(){return id;}
public void setId( int id){ this.id = id;}
public String getNombre(){ return nombre;}
public void setNombre(String nombre){ this.nombre= nombre;}
public int getCantidad(){ return cantidad;}
public void setCantidad (int cantidad){ this.cantidad=cantidad;}
public double getPrecio(){ return precio;}
public void setPrecio (double precio){ this.precio=precio;}
public String getCategoria(){ return categoria;}
public void setCategoria (String categoria){ this.categoria=categoria;}
}