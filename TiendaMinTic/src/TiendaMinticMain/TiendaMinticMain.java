/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TiendaMinticMain;
import Controlador.ControladorUsuario;
import Modelo.ConsultaUsuario;
import Modelo.Usuario;
import Vistas.IngresoUsuario;


public class TiendaMinticMain {
    
    public static void main(String[] args){

        
        
        IngresoUsuario ventanaIngreso = new IngresoUsuario();
        Usuario usuario1 = new Usuario();
        Usuario usuario2=new Usuario();
        ConsultaUsuario modeloUsuario = new ConsultaUsuario();
        ConsultaUsuario modeloUsuario2 = new ConsultaUsuario();
        ControladorUsuario controladorUsuario = new ControladorUsuario(ventanaIngreso, usuario1, modeloUsuario, usuario2, modeloUsuario2);
        controladorUsuario.iniciar();
        ventanaIngreso.setVisible(true);
    }
    
    
}
