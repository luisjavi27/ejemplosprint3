/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;


import Vistas.Ventana;
import Vistas.IngresoUsuario;
import com.mysql.jdbc.Connection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Modelo.*;
import java.sql.ResultSetMetaData;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;



public class ControladorProducto implements ActionListener {
    
    private Ventana vista;
    private Producto producto;
    private ConsultaProducto modelo;
    
    
    public ControladorProducto(Ventana vista, Producto producto, ConsultaProducto modelo){
        
        this.vista =vista;
        this.producto= producto;
        this.modelo= modelo;
        
        
        vista.botonSaveRegistrar.addActionListener(this);
        vista.botonConsultarConsultar.addActionListener(this);
        vista.botonActualizarConsultar.addActionListener(this);
        vista.botonEliminarConsultar.addActionListener(this);
        vista.botonLimpiarConsultar.addActionListener(this);
        
    }
    
    public void iniciar(){
        
        vista.setTitle("Supermercado El Futuro - Inventario");
        
        vista.setLocationRelativeTo(null);
        actualizarTabla();
        
    }
            
    
@Override
    public void actionPerformed(ActionEvent ae){
        
        
        //boton limpiar
        if(ae.getSource()== vista.botonLimpiarConsultar){
            
            limpiarCajas();
            
        }
       
        
        // boton guardar
        if(ae.getSource()== vista.botonSaveRegistrar){
            
            if (vista.cajaNombreRegistrar.getText().equals("") || (vista.cajaCantidadRegistrar.getText().equals("")) || (vista.cajaPrecioRegistrar.getText().equals("")) || (vista.cbCategoriaRegistrar.getSelectedItem().equals("Seleccione..."))) {
            javax.swing.JOptionPane.showMessageDialog(vista,"Debe llenar todos los campos \n","AVISO",javax.swing.JOptionPane.INFORMATION_MESSAGE);
            vista.cajaNombreRegistrar.requestFocus();
            }
            else{
                producto.setNombre(vista.cajaNombreRegistrar.getText());
                producto.setCantidad(Integer.parseInt(vista.cajaCantidadRegistrar.getText()));
                producto.setPrecio(Double.parseDouble(vista.cajaPrecioRegistrar.getText()));

                if(vista.cbCategoriaRegistrar.getSelectedItem()=="ASEO"){
                    producto.setCategoria("ASEO");
                }
                else if (vista.cbCategoriaRegistrar.getSelectedItem()=="VIVERES"){
                    producto.setCategoria("VIVERES");
               
                }

                if (modelo.ingresar(producto)){
                    JOptionPane.showMessageDialog(null,"Registro ingresado correctamente");
                }
                else{
                JOptionPane.showMessageDialog(null ,"Error al ingresar el Registro");
                }
                vista.cajaNombreRegistrar.requestFocus();
                
                limpiarCajas();
                actualizarTabla();
            }
        }


        //Boton Actualizar
        if (ae.getSource()== vista.botonActualizarConsultar){
            
            if (vista.cajaIdConsultar.getText().equals("") ) {
                    javax.swing.JOptionPane.showMessageDialog(vista,"Debe llenar el campo id \n","AVISO",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                    vista.cajaIdConsultar.requestFocus();
                }
            else{
                
                producto.setId(Integer.parseInt(vista.cajaIdConsultar.getText()));
                
                producto.setNombre(("".equals(vista.cajaNombreConsultar.getText()))? "":vista.cajaNombreConsultar.getText());
    
                producto.setCantidad(("".equals(vista.cajaCantidadConsultar.getText()))? -1:Integer.parseInt(vista.cajaCantidadConsultar.getText()));
      
                producto.setPrecio(("".equals(vista.cajaPrecioConsultar.getText()))? -1:Double.parseDouble(vista.cajaCantidadConsultar.getText()));
              
                producto.setCategoria(("Seleccione...".equals(vista.cbCategoriaConsultar.getSelectedItem()))? "":(String)vista.cbCategoriaConsultar.getSelectedItem());
                
                if (modelo.actualizar(producto)){
                    JOptionPane.showMessageDialog(null, "Registro actualizado correctamente");
                    limpiarCajas();
                    actualizarTabla();
                    }
                else{
                    JOptionPane.showMessageDialog(null, "Error al actualizar el registro");
                    
                    
                }
            }
        }

        //Boton consultar
        if(ae.getSource()== vista.botonConsultarConsultar){
            
            actualizarTabla();
            
            if(!"".equals(vista.cajaIdConsultar.getText())){
                producto.setId(Integer.parseInt(vista.cajaIdConsultar.getText()));

                    if(modelo.buscar(producto)){
                        
                        actualizarTablaFiltro( vista.cajaNombreConsultar.getText(), String.valueOf(vista.cbCategoriaConsultar.getSelectedItem()));
                        
                        vista.cajaNombreConsultar.setText(producto.getNombre());
                        vista.cajaCantidadConsultar.setText(String.valueOf(producto.getCantidad()));
                        vista.cajaPrecioConsultar.setText(String.valueOf(producto.getPrecio()));
                        
                        for (int i=0; i<vista.cbCategoriaConsultar.getItemCount(); i++){
                            if (vista.cbCategoriaConsultar.getItemAt(i).equals(producto.getCategoria())){
                                vista.cbCategoriaConsultar.setSelectedIndex(i);
                                
                            }
                        }
                            
                        
                }

                    else{
                        JOptionPane.showMessageDialog(null, "Registro no encontrado");
                        limpiarCajas();
                    }

                }
            
            else if(!"Seleccione...".equals(vista.cbCategoriaConsultar.getSelectedItem())){
                
                
                actualizarTablaFiltro(vista.cajaNombreConsultar.getText(), String.valueOf(vista.cbCategoriaConsultar.getSelectedItem()));
                
            }
            
            else if(!"".equals(vista.cajaNombreConsultar.getText())){
                
               
                actualizarTablaFiltro( vista.cajaNombreConsultar.getText(), String.valueOf(vista.cbCategoriaConsultar.getSelectedItem()));
                
            }
            
                
            }


            // boton eliminar
            if (ae.getSource()== vista.botonEliminarConsultar){
                
                if (vista.cajaIdConsultar.getText().equals("") ) {
                    javax.swing.JOptionPane.showMessageDialog(vista,"Debe llenar el campo id \n","AVISO",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                    vista.cajaIdConsultar.requestFocus();
                }
                else{
                    producto.setId(Integer.parseInt(vista.cajaIdConsultar.getText()));
                    
                    if (modelo.eliminar(producto)){
                        JOptionPane.showMessageDialog(null, "Registro actualizado correctamente");
                        }
                    else{
                        JOptionPane.showMessageDialog(null, "Error al actualizar el registro");
                    }

                    limpiarCajas();
                    actualizarTabla();
                }       
            }
        
    }
    
    
    
    public void limpiarCajas(){
        vista.cajaNombreConsultar.setText(null);
        vista.cajaCantidadConsultar.setText(null);
        vista.cajaIdConsultar.setText(null);
        vista.cajaPrecioConsultar.setText(null);
        vista.cbCategoriaConsultar.setSelectedIndex(0);
        
        vista.cajaNombreRegistrar.setText(null);
        vista.cajaCantidadRegistrar.setText(null);
        vista.cajaPrecioRegistrar.setText(null);
        vista.cbCategoriaRegistrar.setSelectedIndex(0);
        
             
    }
    
    public void actualizarTabla(){
        
        DefaultTableModel modeloTabla = new DefaultTableModel();
        vista.tableConsultar.setModel(modeloTabla);
        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();//
        tcr.setHorizontalAlignment(SwingConstants.CENTER);//

        PreparedStatement ps= null;
        ResultSet rs= null;

        try{
            conexionDb con = new conexionDb();
            Connection conexion = con.getConnection();

            ps = conexion.prepareStatement("select id, nombre, cantidad, precio, categoria from producto");
            rs =ps.executeQuery();
            modeloTabla.addColumn("Id");
            modeloTabla.addColumn("Nombre");
            modeloTabla.addColumn("Cantidad");
            modeloTabla.addColumn("Precio");
            modeloTabla.addColumn("Categoria");

            ResultSetMetaData rsMD =rs.getMetaData();
            int numCols=rsMD.getColumnCount();// Conteo de columnas en la tabla


            while(rs.next()){
                Object fila[]= new Object[numCols];

                for (int i=0; i<numCols; i++){
                    fila[i]=rs.getObject(i+1);
                    vista.tableConsultar.getColumnModel().getColumn(i).setCellRenderer(tcr);//
                }
            modeloTabla.addRow (fila);
            

            }
        }catch(Exception except){
            System.out.println("Error "+ except);
        }

    }
    
    
    public void actualizarTablaFiltro( String fNombre, String fCategoria){
        
        DefaultTableModel modeloTabla = new DefaultTableModel();
        vista.tableConsultar.setModel(modeloTabla);
        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();//
        tcr.setHorizontalAlignment(SwingConstants.CENTER);//

        PreparedStatement ps= null;
        ResultSet rs= null;

        try{
            conexionDb con = new conexionDb();
            Connection conexion = con.getConnection();
            
            ps = conexion.prepareStatement("select * from producto where nombre like '"+fNombre+"%' or categoria like '"+fCategoria+"%'");

            rs =ps.executeQuery();
            modeloTabla.addColumn("Id");
            modeloTabla.addColumn("Nombre");
            modeloTabla.addColumn("Cantidad");
            modeloTabla.addColumn("Precio");
            modeloTabla.addColumn("Categoria");

            ResultSetMetaData rsMD =rs.getMetaData();
            int numCols=rsMD.getColumnCount();// Conteo de columnas en la tabla


            while(rs.next()){
                Object fila[]= new Object[numCols];

                for (int i=0; i<numCols; i++){
                    fila[i]=rs.getObject(i+1);
                    vista.tableConsultar.getColumnModel().getColumn(i).setCellRenderer(tcr);//
                }
            modeloTabla.addRow (fila);
            
            }
        }catch(Exception except){
            System.out.println("Error "+ except);
        }

    }
    
    
    
}            
    

