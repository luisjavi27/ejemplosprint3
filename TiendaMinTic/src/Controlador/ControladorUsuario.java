/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vistas.Ventana;
import Vistas.IngresoUsuario;
import Modelo.ConsultaUsuario;
import Modelo.Usuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import Modelo.*;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author c.comercial4
 */
public class ControladorUsuario  implements ActionListener {
    
    private IngresoUsuario login;
    private Usuario usuario;
    private Usuario usuario2;
    private ConsultaUsuario modeloUsuario;
    Ventana ventana1 = new Ventana();
    Producto producto = new Producto();
   

    public ControladorUsuario(IngresoUsuario login, Usuario usuario, ConsultaUsuario modeloUsuario, Usuario usuario2, ConsultaUsuario modeloUsuario2) {
        
        this.login = login;
        this.usuario=usuario;
        this.usuario2=usuario2;
        this.modeloUsuario= modeloUsuario;
        
        
        login.botonIngresar.addActionListener(this);
        ventana1.botonConsultarU.addActionListener(this);
        ventana1.botonActualizarU.addActionListener(this);
        ventana1.botonEliminarU.addActionListener(this);
        ventana1.nuevoU.addActionListener(this);
        
    }
    
    public void iniciar(){

     login.setTitle("Ingreso");
     login.setLocationRelativeTo(null);
     
    }
   
      public void limpiarCajas(){
   
            login.txtUsuario.setText(null);
            login.txtClave.setText(null);
            
            
        }  

      @Override
    public void actionPerformed(ActionEvent e) {

        
        // boton nuevo usuario
        if(e.getSource()== ventana1.nuevoU){
            
            if (ventana1.cajaUsuarioU.getText().equals("") || (ventana1.cajaClaveU.getText().equals("")) ||  (ventana1.ComboTipoU.getSelectedItem().equals("Seleccione..."))) {
            javax.swing.JOptionPane.showMessageDialog(ventana1,"Debe llenar todos los campos \n","AVISO",javax.swing.JOptionPane.INFORMATION_MESSAGE);
            ventana1.cajaUsuarioU.requestFocus();
            ventana1.cajaIdU.setText(null);
            }
            else{
                usuario.setUsuario(ventana1.cajaUsuarioU.getText());
                usuario.setClave(ventana1.cajaClaveU.getText());
                

                if(ventana1.ComboTipoU.getSelectedItem()=="Administrador"){
                    usuario.setTipo("Administrador");
                }
                else if(ventana1.ComboTipoU.getSelectedItem()=="Operador"){
                    usuario.setTipo("Operador");
                }
                
                else{
                JOptionPane.showMessageDialog(null ,"Error al ingresar el Registro");
                }
                if (modeloUsuario.ingresarNuevoU(usuario)){
                    JOptionPane.showMessageDialog(null,"Registro ingresado correctamente");
                }
                else{
                JOptionPane.showMessageDialog(null ,"Error al ingresar el Registro");
                }
                
                ventana1.cajaUsuarioU.requestFocus();
                limpiarCajasU();
                actualizarTablaUsuarios();
            }
        }
        
//Boton ingresar
        if(e.getSource()== login.botonIngresar){
            
            
            if(!"".equals(login.txtUsuario.getText()) && !"".equals(login.txtClave.getText())){
                
                        usuario2.setUsuario(login.txtUsuario.getText());
                        usuario2.setClave(login.txtClave.getText());
                        

                    if(modeloUsuario.buscar(usuario2)){
                        
                        ConsultaProducto modelo = new ConsultaProducto();
                        ControladorProducto controlador = new ControladorProducto(ventana1, producto, modelo);
                        controlador.iniciar();
                        login.dispose();
                        ventana1.setVisible(true);
                        
                        
                        modeloUsuario.buscarU(usuario2);
                        
                        
                        if("Operador".equals(usuario2.getTipo())){
                             userOper();
                             System.out.println("user:"+usuario2.getTipo());
                                                     }
                        actualizarTablaUsuarios();
                    }
                    
                    else{
                        JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrecto");
                        login.txtUsuario.requestFocus();
                        limpiarCajas();
                    }
                        
                        
                }

                    else{
                        JOptionPane.showMessageDialog(null, "Ingrese su usuario y contraseña");
                        login.txtUsuario.requestFocus();
                        limpiarCajas();
                    }

                }
        
        //Boton consultar
        if(e.getSource()== ventana1.botonConsultarU){
            
            
            
            if(!"".equals(ventana1.cajaIdU.getText())){
                usuario.setId(Integer.parseInt(ventana1.cajaIdU.getText()));
                
                    if(modeloUsuario.buscarU(usuario)){
                        
                        ventana1.cajaUsuarioU.setText(usuario.getUsuario());
                        
                        
                        for (int i=0; i<ventana1.ComboTipoU.getItemCount(); i++){
                            if (ventana1.ComboTipoU.getItemAt(i).equals(usuario.getTipo())){
                                ventana1.ComboTipoU.setSelectedIndex(i);
                            }
                        }
                        
                }

                    else{
                        JOptionPane.showMessageDialog(null, "Registro no encontrado");
                        limpiarCajasU();
                    }

                }
            
            
                actualizarTablaUsuarios();
            }
        
            //Boton Actualizar
        if (e.getSource()== ventana1.botonActualizarU){
            
            if (ventana1.cajaIdU.getText().equals("") ) {
                    javax.swing.JOptionPane.showMessageDialog(ventana1,"Debe llenar el campo id \n","AVISO",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                    ventana1.cajaIdU.requestFocus();
                }
            else{
                
                usuario.setId(Integer.parseInt(ventana1.cajaIdU.getText()));
                usuario.setUsuario(ventana1.cajaUsuarioU.getText());
                usuario.setClave(ventana1.cajaClaveU.getText());

                usuario.setTipo(("Seleccione...".equals(ventana1.ComboTipoU.getSelectedItem()))? "":(String)ventana1.ComboTipoU.getSelectedItem());
                 
                if (modeloUsuario.actualizarU(usuario)){
                    JOptionPane.showMessageDialog(null, "Registro actualizado correctamente");
                    limpiarCajasU();
                    actualizarTablaUsuarios();
                    }
                else{
                    JOptionPane.showMessageDialog(null, "Error al actualizar el registro");
                }
            }
        }
        
        // boton eliminar
            if (e.getSource()== ventana1.botonEliminarU){
                
                if (ventana1.cajaIdU.getText().equals("") ) {
                    javax.swing.JOptionPane.showMessageDialog(ventana1,"Debe llenar el campo id \n","AVISO",javax.swing.JOptionPane.INFORMATION_MESSAGE);
                    ventana1.cajaIdU.requestFocus();
                }
                else{
                    usuario.setId(Integer.parseInt(ventana1.cajaIdU.getText()));
                    
                    if (modeloUsuario.eliminarU(usuario)){
                        JOptionPane.showMessageDialog(null, "Registro actualizado correctamente");
                        }
                    else{
                        JOptionPane.showMessageDialog(null, "Error al actualizar el registro");
                    }

                    limpiarCajasU();
                    actualizarTablaUsuarios();
                }       
            }
        
    }
    
     public void actualizarTablaUsuarios(){
        
        if ("Administrador".equals(usuario2.getTipo())){
       
        DefaultTableModel modeloTabla = new DefaultTableModel();
        ventana1.tablaUsuariosUsuarios.setModel(modeloTabla);
        DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();//
        tcr.setHorizontalAlignment(SwingConstants.CENTER);//

        PreparedStatement ps= null;
        ResultSet rs= null;

        try{
            conexionDb con = new conexionDb();
            Connection conexion = con.getConnection();

            ps = conexion.prepareStatement("select idusuarios, usuario, tipousuario from usuarios");
            rs =ps.executeQuery();
            modeloTabla.addColumn("Id");// Agregar columna Id a la tabla
            modeloTabla.addColumn("Usuario");//Agregar columna Edad a la tabla
            modeloTabla.addColumn("Tipo");//Agregar columna Vacuna a la tabla

            ResultSetMetaData rsMD =rs.getMetaData();
            int numCols=rsMD.getColumnCount();// Conteo de columnas en la tabla


            while(rs.next()){
                Object fila[]= new Object[numCols];

                for (int i=0; i<numCols; i++){
                    fila[i]=rs.getObject(i+1);
                    ventana1.tablaUsuariosUsuarios.getColumnModel().getColumn(i).setCellRenderer(tcr);//
                }
            modeloTabla.addRow (fila);

            }
        }catch(Exception except){
            System.out.println("Error "+ except);
        }
        
        }
    }
     
     public void userOper(){
        ventana1.botonEliminarConsultar.setEnabled(false);
        ventana1.botonEliminarU.setEnabled(false);
        ventana1.nuevoU.setEnabled(false);
        ventana1.tablaUsuariosUsuarios.setEnabled(false);
        ventana1.cajaIdU.setEnabled(false);
        ventana1.cajaUsuarioU.setEnabled(false);
        ventana1.cajaClaveU.setEnabled(false);
        ventana1.ComboTipoU.setEnabled(false);
        ventana1.botonConsultarU.setEnabled(false);
        ventana1.botonActualizarU.setEnabled(false);
        ventana1.jPanel3.setEnabled(false);
         
     }
     
     public void limpiarCajasU(){
        ventana1.cajaUsuarioU.setText(null);
        ventana1.cajaIdU.setText(null);
        ventana1.cajaClaveU.setText(null);
        
        ventana1.ComboTipoU.setSelectedIndex(0);
          
    }
    
    }
    
     
    
    

